# SMMA IT Developer Test

Selamat Datang di SMMA Test IT Developer Test!

Untuk lebih mengetahui kemampuan Anda dalam membuat aplikasi berbasis web

## Soal

>Anda diminta untuk membuat sebuah website sederhana yang akan menampilkan list dari user yang telah terdaftar di aplikasi perusahaan tempat Anda bekerja. Aplikasi tersebut haruslah memiliki fitur sebagai berikut: 
  >- Minimal terdiri dari 3 halaman
  >- Terdapat fitur login dan logout
  >- Terdapat sidebar
  >- Manampilkan data-data user dari REST API berupa:
  >   - Nama lengkap
  >   - Gender
  >   - Tanggal lahir
  >   - Nomor telpon
  >   - Alamat email
  >- Menampilkan gambar yang bersifat dinamis (untuk halaman yang sama dapat menampilkan gambar yang berbeda-beda)

  Data user dapat menggunakan REST API berikut:
  ```
  https://dummyjson.com/users
  ```

Diperbolehkan menggunakan API lain (contoh: dari database buatan sendiri).

Akan mendapat penilaian lebih jika:
 - Terhubung dengan Back-end dan database
 - Memiliki desain yang bagus
 - Dapat diakses secara public (memiliki alamat website http atau https)

## Petunjuk pengerjaan
 Silahkan clone repositori ini, kemudian buat branch baru dengan ketentuan:
  >nama lengkap anda dipisahkan dengan strip (-) dengan huruf kecil semua (contoh: romli-anwar).
  
  kemudian commit dan push agar branch buatan Anda bisa langsung ter-update.
 
 Batas akhir pengerjaan:

 >Rabu, 7 Juni 2023. Pukul 16.00 WIB

 Dimohon untuk commit dan push pekerjaan Anda ke Git sesering mungkin

 **DILARANG UNTUK PUSH KE BRANCH MASTER**

Mohon  periksa setiap kali anda melakukan push pekerjaan Anda.
Jika sudah selesai lakukan pull request.

Pastikan setidaknya aplikasi web Anda dapat dijalankan di localhost komputer manapun

### Selamat mengerjakan!

